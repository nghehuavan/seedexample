﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using SeedExample.EfContext;
using System;

namespace SeedExample.Migrations
{
    public class Program
    {

        static SeedExampleContext dbContext;
        public static void Main(string[] args)
        {
            dbContext = new MigrationFactoryContext().Create(new DbContextFactoryOptions());
            Seed();
            dbContext.SaveChanges();
            Console.Read();
        }

        private static void Seed()
        {
            dbContext.ToDos.Add(new ToDo { Id = new Guid(), Content = "Some Todo content" });
            Console.WriteLine("Implement your seed code here [Program.cs]-> Seed()!!");
        }
    }
}
