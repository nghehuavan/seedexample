﻿using System;

namespace SeedExample.EfContext
{
    public class ToDo
    {
        public Guid Id { get; set; }

        public string Content { get; set; }
    }
}
