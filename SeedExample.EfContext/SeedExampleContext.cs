﻿using Microsoft.EntityFrameworkCore;

namespace SeedExample.EfContext
{
    public class SeedExampleContext : DbContext
    {
        public SeedExampleContext(DbContextOptions options) : base(options)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDo>().ToTable("app_ToDo");
            modelBuilder.Entity<ToDo>().HasKey(x => x.Id);
        }

        public DbSet<ToDo> ToDos { get; set; }
    }
}
