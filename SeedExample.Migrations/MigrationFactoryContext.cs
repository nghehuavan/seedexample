﻿using SeedExample.EfContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace SeedExample.Migrations
{
    /// <summary>
    /// This is trick when run command of enttity framework core with class library 
    /// Reason : Entityframwork Core Not Support Class Library
    /// Command List : dotnet ef migrations add {MigrationName}
    ///                dotnet ef migrations remove
    ///                dotnet ef database update 
    /// </summary>
    public class MigrationFactoryContext : IDbContextFactory<SeedExampleContext>
    {
        public SeedExampleContext Create(DbContextFactoryOptions options)
        {
            var connectionString = "Data Source=(local);Initial Catalog=SeedExample;Integrated Security=true";
            var builder = new DbContextOptionsBuilder<SeedExampleContext>();
            builder.UseSqlServer(connectionString, b => b.MigrationsAssembly(this.GetType().Namespace));
            return new SeedExampleContext(builder.Options);
        }
    }
}